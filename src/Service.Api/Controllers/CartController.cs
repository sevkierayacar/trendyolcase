﻿using Core.Logic.Logging;
using Core.Logic.Model.Entities;
using Core.Logic.Model.Exceptions;
using Core.Logic.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Service.Api.Controllers
{
    [RoutePrefix("Cart")]
    public class CartController : ApiController
    {
        #region Fields

        private readonly ICategoryRepository _categoryRepository;
        private readonly ICartManager _cartManager;

        #endregion

        #region Constructors

        public CartController(ICategoryRepository categoryRepository, ICartManager cartManager)
        {
            _categoryRepository = categoryRepository;
            _cartManager = cartManager;
        }

        #endregion

        #region MvcActions

        [HttpPost]
        [Route("CalculateCart")]
        public async Task<HttpResponseMessage> CalculateCart([FromBody]List<CartItem> request)
        {
            var result = new CartCalculationResponse();
            var httpStatusCode = HttpStatusCode.OK;

            try
            {
                foreach (var cartItem in request)
                {
                    cartItem.Category = await _categoryRepository.FilterByProductIdAsync(cartItem.ProductId);
                    cartItem.Product = cartItem.Category
                        .CategoryProductMaps
                        .Where(c => c.ProductId == cartItem.ProductId)
                        .Select(c => c.Product)
                        .First();
                    cartItem.FinalPrice = cartItem.Product.Price * cartItem.Quantity;
                }

                var initialPrice = request.Sum(c => c.FinalPrice);
                var cartItems = await _cartManager.ApplyCampaigns(request);
                cartItems = await _cartManager.ApplyCoupons(cartItems);

                result.DeliveryPrice = _cartManager.GetDeliveryPrice(50, 25, 2.99, cartItems);

                result.CartItems.AddRange(cartItems.Select(c => new CartItemResponse
                {
                    FinalPrice = c.FinalPrice,
                    ProductId = c.ProductId,
                    Quantity = c.Quantity,
                    CategoryTitle = c.Category.Title,
                    ProductPrice = c.Product.Price,
                    ProductTitle = c.Product.Title
                }));
                result.FinalTotalPrice = cartItems.Sum(c => c.FinalPrice) + result.DeliveryPrice;
                result.TotalAppliedDiscount = initialPrice - cartItems.Sum(c => c.FinalPrice);
            }
            catch (RepositoryException ex)
            {
                httpStatusCode = HttpStatusCode.InternalServerError;
                result.Message = "Cannot get calculate due to database issues!";

                SharpLogger.LogError(ex);
            }
            catch (ManagerException ex)
            {
                httpStatusCode = HttpStatusCode.InternalServerError;
                result.Message = "Cannot get calculate cart!";

                SharpLogger.LogError(ex);
            }
            catch (Exception ex)
            {
                httpStatusCode = HttpStatusCode.InternalServerError;
                result.Message = "An unknown error occurred!";

                SharpLogger.LogError(ex);
            }

            return Request.CreateResponse(httpStatusCode, result);
        }

        #endregion
    }
}
