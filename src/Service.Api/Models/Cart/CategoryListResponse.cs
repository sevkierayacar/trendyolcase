﻿using System.Collections.Generic;
using Core.Logic.Model.Entities;
using Service.Api.AppCode;

namespace Service.Api.Models.Cart
{
    public class CategoryListResponse : ResponseBase
    {
        public ICollection<Category> Categories { get; set; }
    }
}