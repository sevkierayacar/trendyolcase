﻿using Core.Logic.Logging;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Service.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            SharpLogger.Start();
            SharpLogger.LogInfo("Application started!");

        }
        protected void Application_Error(object sender, EventArgs e)
        {
            SharpLogger.LogError(Server.GetLastError());
        }
    }
}