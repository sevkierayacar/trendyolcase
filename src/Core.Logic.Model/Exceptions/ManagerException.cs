﻿using System;

namespace Core.Logic.Model.Exceptions
{
    [Serializable]
    public class ManagerException : Exception
    {
        #region Contructors

        public ManagerException()
        {
        }
        public ManagerException(string message) : base(message)
        {
        }
        public ManagerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        #endregion
    }
}