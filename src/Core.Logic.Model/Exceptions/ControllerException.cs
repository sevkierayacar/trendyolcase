﻿using System;

namespace Core.Logic.Model.Exceptions
{
    [Serializable]
    public class ControllerException : Exception
    {
        #region Contructors

        public ControllerException()
        {
        }
        public ControllerException(string message) : base(message)
        {
        }
        public ControllerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        #endregion
    }
}