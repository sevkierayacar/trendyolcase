﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Logic.Model.Entities;

namespace Core.Logic.Model.Interfaces
{
    public interface ICartManager : INinject
    {
        Task<ICollection<CartItem>> ApplyCampaigns(ICollection<CartItem> cartItems);
        Task<ICollection<CartItem>> ApplyCoupons(ICollection<CartItem> cartItems);
        double GetDeliveryPrice(double costPerDelivery, double costPerProduct, double fixedPrice, ICollection<CartItem> cartItems);
    }
}