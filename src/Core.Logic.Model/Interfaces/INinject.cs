﻿namespace Core.Logic.Model.Interfaces
{
    /// <summary>
    /// Only used as a base interface to easier instantiation
    /// </summary>
    public interface INinject
    {
    }
}