﻿using Core.Logic.Model.Entities;
using System;

namespace Core.Logic.Model.Interfaces
{
    public interface IWebClientManager : IDisposable, INinject
    {
        string UploadString(string address, string method, string data);
        PatientWebClient PatientWebClient { get; }
    }
}