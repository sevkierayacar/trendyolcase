﻿namespace Core.Logic.Model.Interfaces
{
    public interface IContextDataSeeder : INinject
    {
        void Seed();
    }
}