﻿using System.Collections.Generic;
using Core.Logic.Model.Entities;
using System.Threading.Tasks;

namespace Core.Logic.Model.Interfaces
{
    public interface ICouponRepository : INinject
    {
        Task<ICollection<Coupon>> FilterByHighestCoupon(double currentPrice);
    }
}