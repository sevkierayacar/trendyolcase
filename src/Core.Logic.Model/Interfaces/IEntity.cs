﻿namespace Core.Logic.Model.Interfaces
{
    public interface IEntity<T> : IEntityBase
    {
        #region Properties

        T Id { get; set; }

        #endregion
    }
}