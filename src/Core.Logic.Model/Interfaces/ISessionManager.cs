﻿namespace Core.Logic.Model.Interfaces
{
    public interface ISessionManager : INinject
    {
        string Username { get; set; }
    }
}