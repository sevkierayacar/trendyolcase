﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Core.Logic.Model.Entities;

namespace Core.Logic.Model.Interfaces
{
    public interface IMainDataContext : IDisposable, INinject
    {
        #region Properties

        DbSet<Campaign> Campaigns { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<CategoryCampaignMap> CategoryCampaignMaps { get; set; }
        DbSet<CategoryProductMap> CategoryProductMaps { get; set; }
        DbSet<Coupon> Coupons { get; set; }
        DbSet<Product> Products { get; set; }

        #endregion

        #region Methods

        int SaveChanges();
        Task<int> SaveChangesAsync();

        #endregion
    }
}
