﻿using System;

namespace Core.Logic.Model.Interfaces
{
    public interface IEntityBase
    {
        #region Properties

        DateTime CreatedOn { get; set; }
        DateTime ModifiedOn { get; set; }

        #endregion

        #region Methods

        bool IsValid();

        #endregion
    }
}