﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Logic.Model.Entities;

namespace Core.Logic.Model.Interfaces
{
    public interface ICampaignRepository : INinject
    {
        Task<ICollection<Campaign>> Filter(IEnumerable<int> productIds);
    }
}