﻿using Core.Logic.Model.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Logic.Model.Interfaces
{
    public interface IProductRepository : INinject
    {
        Task<ICollection<Product>> Filter(IEnumerable<int> productIds);
    }
}