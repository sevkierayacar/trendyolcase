﻿namespace Core.Logic.Model.Interfaces
{
    public interface IWebClientFactory
    {
        IWebClientManager CreateWebClientManager();
    }
}