﻿using Core.Logic.Model.Entities;
using System.Threading.Tasks;

namespace Core.Logic.Model.Interfaces
{
    public interface ICategoryRepository : INinject
    {
        Task<Category> FilterByProductIdAsync(int productId);
    }
}