﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System.Data.Entity.Migrations;
using Core.Logic.Model.Enums;

namespace Core.Logic.Model.Migrations.Seeders
{
    public class CouponSeeder : IContextDataSeeder
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public CouponSeeder(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - IContextDataSeeder

        public void Seed()
        {
            _context.Coupons.AddOrUpdate
            (
                new Coupon { Id = 1, Title = "For at least 100TL, 5% Discount", MinimumPrice = 100, DiscountAmount = 5, DiscountType = DiscountType.Rate },
                new Coupon { Id = 2, Title = "For at least 200TL, 20TL Discount", MinimumPrice = 200, DiscountAmount = 20, DiscountType = DiscountType.Amount },
                new Coupon { Id = 3, Title = "For at least 500TL, 10% Discount", MinimumPrice = 500, DiscountAmount = 10, DiscountType = DiscountType.Rate },
                new Coupon { Id = 4, Title = "For at least 750TL, 150TL Discount", MinimumPrice = 750, DiscountAmount = 150, DiscountType = DiscountType.Amount },
                new Coupon { Id = 5, Title = "For at least 1500TL, 25% Discount", MinimumPrice = 1500, DiscountAmount = 25, DiscountType = DiscountType.Rate }
            );
        }

        #endregion
    }
}