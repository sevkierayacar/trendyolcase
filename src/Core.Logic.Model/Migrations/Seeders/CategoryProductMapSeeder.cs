﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System.Data.Entity.Migrations;

namespace Core.Logic.Model.Migrations.Seeders
{
    public class CategoryProductMapSeeder : IContextDataSeeder
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public CategoryProductMapSeeder(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - IContextDataSeeder

        public void Seed()
        {
            _context.CategoryProductMaps.AddOrUpdate
            (
                new CategoryProductMap { CategoryId = 2, ProductId = 1 },
                new CategoryProductMap { CategoryId = 2, ProductId = 2 },
                new CategoryProductMap { CategoryId = 2, ProductId = 3 },
                new CategoryProductMap { CategoryId = 2, ProductId = 4 },

                new CategoryProductMap { CategoryId = 3, ProductId = 5 },
                new CategoryProductMap { CategoryId = 3, ProductId = 6 },
                new CategoryProductMap { CategoryId = 3, ProductId = 7 },

                new CategoryProductMap { CategoryId = 5, ProductId = 8 },
                new CategoryProductMap { CategoryId = 5, ProductId = 9 },
                new CategoryProductMap { CategoryId = 5, ProductId = 10 },

                new CategoryProductMap { CategoryId = 7, ProductId = 11 },
                new CategoryProductMap { CategoryId = 7, ProductId = 12 }
            );
        }

        #endregion
    }
}