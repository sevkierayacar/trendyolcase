﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace Core.Logic.Model.Migrations.Seeders
{
    public class CategorySeeder : IContextDataSeeder
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public CategorySeeder(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - IContextDataSeeder

        public void Seed()
        {
            #region Food

            _context.Categories.AddOrUpdate(new Category
            {
                Id = 1,
                Title = "Food",
                Children = new HashSet<Category>
                {
                    new Category
                    {
                        Id = 2,
                        Title = "Fruits"
                    },
                    new Category
                    {
                        Id = 3,
                        Title = "Sweets"
                    }
                }
            });

            #endregion

            #region Cloth

            _context.Categories.AddOrUpdate(new Category
            {
                Id = 4,
                Title = "Cloth",
                Children = new HashSet<Category>
                {
                    new Category
                    {
                        Id = 5,
                        Title = "Pants",
                    }
                }
            });

            #endregion

            #region Techonology

            _context.Categories.AddOrUpdate(new Category
            {
                Id = 6,
                Title = "Techonology",
                Children = new HashSet<Category>
                {
                    new Category
                    {
                        Id = 7,
                        Title = "Phones",
                    }
                }
            });

            #endregion
        }

        #endregion
    }
}