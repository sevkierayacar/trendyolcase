﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System.Data.Entity.Migrations;

namespace Core.Logic.Model.Migrations.Seeders
{
    public class ProductSeeder : IContextDataSeeder
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public ProductSeeder(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - IContextDataSeeder

        public void Seed()
        {
            #region Fruits

            _context.Products.AddOrUpdate(
                new Product { Id = 1, Title = "Apple", Price = 1.0 },
                new Product { Id = 2, Title = "Avocado", Price = 2.2 },
                new Product { Id = 3, Title = "Apricot", Price = 2.5 },
                new Product { Id = 4, Title = "Banana", Price = 1.5 }
            );

            #endregion

            #region Sweets

            _context.Products.AddOrUpdate(
                new Product { Id = 5, Title = "Wonka", Price = 3.0 },
                new Product { Id = 6, Title = "Tootsie", Price = 5.0 },
                new Product { Id = 7, Title = "Jolly Rancher", Price = 12.5 }
            );

            #endregion

            #region Pants

            _context.Products.AddOrUpdate(
                new Product { Id = 8, Title = "Adrianna", Price = 213.0 },
                new Product { Id = 9, Title = "Abby Z.", Price = 105.0 },
                new Product { Id = 10, Title = "Acorn", Price = 500.5 }
            );

            #endregion

            #region Phones

            _context.Products.AddOrUpdate(
                new Product { Id = 11, Title = "iPhone", Price = 5000.0 },
                new Product { Id = 12, Title = "Android.", Price = 350.0 }
            );

            #endregion
        }

        #endregion
    }
}