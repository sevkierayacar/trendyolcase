﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System.Collections.Generic;
using System.Data.Entity.Migrations;

namespace Core.Logic.Model.Migrations.Seeders
{
    public class CategoryCampaignMapSeeder : IContextDataSeeder
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public CategoryCampaignMapSeeder(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - IContextDataSeeder

        public void Seed()
        {
            _context.CategoryCampaignMaps.AddOrUpdate
            (
                new CategoryCampaignMap { CategoryId = 1, CampaignId = 1 },
                new CategoryCampaignMap { CategoryId = 2, CampaignId = 3 },
                new CategoryCampaignMap { CategoryId = 2, CampaignId = 5 },
                new CategoryCampaignMap { CategoryId = 2, CampaignId = 6 },
                new CategoryCampaignMap { CategoryId = 3, CampaignId = 2 },
                new CategoryCampaignMap { CategoryId = 3, CampaignId = 3 },
                new CategoryCampaignMap { CategoryId = 5, CampaignId = 4 },
                new CategoryCampaignMap { CategoryId = 5, CampaignId = 5 },
                new CategoryCampaignMap { CategoryId = 6, CampaignId = 7 }
            );
        }

        #endregion
    }
}