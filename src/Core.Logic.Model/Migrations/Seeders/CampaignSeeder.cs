﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System.Data.Entity.Migrations;
using Core.Logic.Model.Enums;

namespace Core.Logic.Model.Migrations.Seeders
{
    public class CampaignSeeder : IContextDataSeeder
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public CampaignSeeder(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - IContextDataSeeder

        public void Seed()
        {
            _context.Campaigns.AddOrUpdate(
                new Campaign { Id = 1, Title = "For at least 20 items, 20% Discount", DiscountAmount = 20, MinimumRequiredQuantity = 10, DiscountType = DiscountType.Rate },
                new Campaign { Id = 2, Title = "For at least 40 items, 25% Discount", DiscountAmount = 25, MinimumRequiredQuantity = 40, DiscountType = DiscountType.Rate },
                new Campaign { Id = 3, Title = "For at least 60 items, 50% Discount", DiscountAmount = 50, MinimumRequiredQuantity = 60, DiscountType = DiscountType.Rate },
                new Campaign { Id = 4, Title = "For at least 20 items, 5TL Discount", DiscountAmount = 5, MinimumRequiredQuantity = 20, DiscountType = DiscountType.Amount },
                new Campaign { Id = 5, Title = "For at least 30 items, 15TL Discount", DiscountAmount = 15, MinimumRequiredQuantity = 30, DiscountType = DiscountType.Amount },
                new Campaign { Id = 6, Title = "For at least 50 items, 50TL Discount", DiscountAmount = 50, MinimumRequiredQuantity = 50, DiscountType = DiscountType.Amount },
                new Campaign { Id = 7, Title = "For at least 20 items, 250TL Discount", DiscountAmount = 250, MinimumRequiredQuantity = 20, DiscountType = DiscountType.Amount }
            );
        }

        #endregion
    }
}