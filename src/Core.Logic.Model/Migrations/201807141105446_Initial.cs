namespace Core.Logic.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Service.Campaign",
                c => new
                    {
                        CampaignId = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 150, unicode: false),
                        DiscountAmount = c.Double(nullable: false),
                        MinimumRequiredQuantity = c.Double(nullable: false),
                        DiscountTypeId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.CampaignId);
            
            CreateTable(
                "Service.Category",
                c => new
                    {
                        CategoryId = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 150, unicode: false),
                        ParentId = c.Int(),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.CategoryId)
                .ForeignKey("Service.Category", t => t.ParentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "Service.CategoryCampaignMap",
                c => new
                    {
                        CategoryCampaignMapId = c.Guid(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        CampaignId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.CategoryCampaignMapId)
                .ForeignKey("Service.Campaign", t => t.CampaignId, cascadeDelete: true)
                .ForeignKey("Service.Category", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.CampaignId);
            
            CreateTable(
                "Service.CategoryProductMap",
                c => new
                    {
                        CategoryProductMapId = c.Guid(nullable: false, identity: true),
                        CategoryId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.CategoryProductMapId)
                .ForeignKey("Service.Category", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("Service.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "Service.Product",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 150, unicode: false),
                        Price = c.Double(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ProductId);
            
            CreateTable(
                "Service.Coupon",
                c => new
                    {
                        CouponId = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 150, unicode: false),
                        DiscountAmount = c.Double(nullable: false),
                        MinimumPrice = c.Double(nullable: false),
                        DiscountTypeId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.CouponId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Service.Category", "ParentId", "Service.Category");
            DropForeignKey("Service.CategoryProductMap", "ProductId", "Service.Product");
            DropForeignKey("Service.CategoryProductMap", "CategoryId", "Service.Category");
            DropForeignKey("Service.CategoryCampaignMap", "CategoryId", "Service.Category");
            DropForeignKey("Service.CategoryCampaignMap", "CampaignId", "Service.Campaign");
            DropIndex("Service.CategoryProductMap", new[] { "ProductId" });
            DropIndex("Service.CategoryProductMap", new[] { "CategoryId" });
            DropIndex("Service.CategoryCampaignMap", new[] { "CampaignId" });
            DropIndex("Service.CategoryCampaignMap", new[] { "CategoryId" });
            DropIndex("Service.Category", new[] { "ParentId" });
            DropTable("Service.Coupon");
            DropTable("Service.Product");
            DropTable("Service.CategoryProductMap");
            DropTable("Service.CategoryCampaignMap");
            DropTable("Service.Category");
            DropTable("Service.Campaign");
        }
    }
}
