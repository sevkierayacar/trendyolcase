using Core.Logic.Model.Interfaces;
using System.Collections.Generic;
using Core.Logic.Model.Migrations.Seeders;

namespace Core.Logic.Model.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Database.Context.MainDataContext>
    {
        #region Fields

        private IEnumerable<IContextDataSeeder> _contextDataSeeders;
        private readonly bool _isSeed;

        #endregion

        #region Constructors

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
            _isSeed = true;
            MigrationsNamespace = GetType().Namespace;
        }

        #endregion

        #region Methods - Protected

        protected override void Seed(Database.Context.MainDataContext context)
        {
            #region Mandatory Seeders

            if (_isSeed)
            {
                _contextDataSeeders = new List<IContextDataSeeder> //Order is important!
                {
                    new ProductSeeder(context),
                    new CampaignSeeder(context),
                    new CategorySeeder(context),
                    new CouponSeeder(context),
                    new CategoryCampaignMapSeeder(context),
                    new CategoryProductMapSeeder(context),
                };

                foreach (var seeder in _contextDataSeeders)
                {
                    seeder.Seed();
                    context.SaveChanges();
                }
            }

            #endregion
        }

        #endregion
    }
}