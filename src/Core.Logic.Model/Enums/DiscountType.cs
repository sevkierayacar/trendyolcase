﻿namespace Core.Logic.Model.Enums
{
    public enum DiscountType
    {
        Rate = 1,
        Amount = 2
    }
}