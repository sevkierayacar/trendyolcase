﻿using System;
using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Logic.Logging;
using Core.Logic.Model.Database.Configurations;
using Core.Logic.Model.Globals.Statics;

namespace Core.Logic.Model.Database.Context
{
    public class MainDataContext : DbContext, IMainDataContext
    {
        #region Properties - IMainDataContext

        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryCampaignMap> CategoryCampaignMaps { get; set; }
        public DbSet<CategoryProductMap> CategoryProductMaps { get; set; }
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<Product> Products { get; set; }

        #endregion

        #region Constructors

        public MainDataContext()
            : base("name=" + DbConnectionStrings.Main)
        {
            System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<MainDataContext>());
            this.Configuration.LazyLoadingEnabled = false;
        }

        #endregion

        #region Methods - Overriden

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new CampaignConfiguration());
            modelBuilder.Configurations.Add(new CategoryCampaignMapConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new CategoryProductMapConfiguration());
            modelBuilder.Configurations.Add(new CouponConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());

        }
        public override int SaveChanges()
        {
            try
            {
                var result = SaveContext();
                if (result >= 0)
                    return result;

                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var eve in ex.EntityValidationErrors)
                {
                    sb.AppendLine($"Entity of type \"{0}\" in state \"{1}\" has the following validation name: {eve.Entry.Entity.GetType().Name} | State: {eve.Entry.State}");
                    foreach (var ve in eve.ValidationErrors)
                    {
                        sb.AppendLine($"Property:{ve.PropertyName} | Error: {ve.ErrorMessage}");
                    }
                }
                SharpLogger.LogError(ex, new
                {
                    Detail = sb.ToString()
                });
                throw;
            }
        }
        public override Task<int> SaveChangesAsync()
        {
            return SaveChangesAsync(CancellationToken.None);
        }
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            var result = SaveContext();
            if (result >= 0)
                return result;

            return await base.SaveChangesAsync(cancellationToken);
        }

        #endregion

        #region Methods - Private

        private int SaveContext()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x =>
                (x.Entity is IEntity<int> || x.Entity is IEntity<Guid> || x.Entity is IEntity<long>)
                && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var dbEntityEntry in modifiedEntries)
            {
                if (dbEntityEntry.Entity is IEntity<Guid> entity)
                {
                    DateTime now = DateTime.UtcNow;

                    if (dbEntityEntry.State == EntityState.Added)
                    {
                        if (entity.CreatedOn == DateTime.MinValue || entity.CreatedOn == DateTime.MaxValue)
                        {
                            entity.CreatedOn = now;
                        }
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedOn).IsModified = false;
                    }

                    entity.ModifiedOn = now;
                }
            }

            return -1;
        }

        #endregion
    }
}
