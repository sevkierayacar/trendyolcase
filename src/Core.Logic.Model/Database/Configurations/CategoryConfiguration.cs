﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Globals.Statics;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Core.Logic.Model.Database.Configurations
{
    public class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            #region Table

            ToTable("Category", DbSchemas.Service);

            #endregion

            #region Properties

            HasKey(c => c.Id);
            Property(c => c.Id).IsRequired().HasColumnName("CategoryId").HasColumnType("INT").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(c => c.Title).IsRequired().HasColumnName("Title").HasColumnType("VARCHAR").HasMaxLength(150);
            Property(c => c.ParentId).IsOptional().HasColumnName("ParentId").HasColumnType("INT");

            #endregion

            #region Relations

            HasOptional(d => d.Parent).WithMany(p => p.Children).HasForeignKey(c => c.ParentId).WillCascadeOnDelete(false);

            #endregion
        }
    }
}