﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Globals.Statics;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Core.Logic.Model.Database.Configurations
{
    public class CategoryProductMapConfiguration : EntityTypeConfiguration<CategoryProductMap>
    {
        public CategoryProductMapConfiguration()
        {
            #region Table

            ToTable("CategoryProductMap", DbSchemas.Service);

            #endregion

            #region Properties

            HasKey(c => c.Id);
            Property(c => c.Id).IsRequired().HasColumnName("CategoryProductMapId").HasColumnType("UNIQUEIDENTIFIER").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.CategoryId).IsRequired().HasColumnName("CategoryId").HasColumnType("INT");
            Property(c => c.ProductId).IsRequired().HasColumnName("ProductId").HasColumnType("INT");

            #endregion
            
        }
    }
}