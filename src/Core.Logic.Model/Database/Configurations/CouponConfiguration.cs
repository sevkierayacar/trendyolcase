﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Globals.Statics;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Core.Logic.Model.Database.Configurations
{
    public class CouponConfiguration : EntityTypeConfiguration<Coupon>
    {
        public CouponConfiguration()
        {
            #region Table

            ToTable("Coupon", DbSchemas.Service);

            #endregion

            #region Properties

            HasKey(c => c.Id);
            Property(c => c.Id).IsRequired().HasColumnName("CouponId").HasColumnType("INT").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(c => c.Title).IsRequired().HasColumnName("Title").HasColumnType("VARCHAR").HasMaxLength(150);
            Property(c => c.MinimumPrice).IsRequired().HasColumnName("MinimumPrice").HasColumnType("FLOAT");
            Property(c => c.DiscountAmount).IsRequired().HasColumnName("DiscountAmount").HasColumnType("FLOAT");
            Property(c => c.DiscountType).IsRequired().HasColumnName("DiscountTypeId").HasColumnType("INT");

            #endregion
        }
    }
}