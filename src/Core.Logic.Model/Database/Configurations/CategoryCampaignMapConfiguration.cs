﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Globals.Statics;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Core.Logic.Model.Database.Configurations
{
    public class CategoryCampaignMapConfiguration : EntityTypeConfiguration<CategoryCampaignMap>
    {
        public CategoryCampaignMapConfiguration()
        {
            #region Table

            ToTable("CategoryCampaignMap", DbSchemas.Service);

            #endregion

            #region Properties

            HasKey(c => c.Id);
            Property(c => c.Id).IsRequired().HasColumnName("CategoryCampaignMapId").HasColumnType("UNIQUEIDENTIFIER").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(c => c.CategoryId).IsRequired().HasColumnName("CategoryId").HasColumnType("INT");
            Property(c => c.CampaignId).IsRequired().HasColumnName("CampaignId").HasColumnType("INT");

            #endregion
        }
    }
}