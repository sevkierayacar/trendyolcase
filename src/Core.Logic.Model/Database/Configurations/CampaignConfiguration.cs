﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Globals.Statics;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Core.Logic.Model.Database.Configurations
{
    public class CampaignConfiguration : EntityTypeConfiguration<Campaign>
    {
        public CampaignConfiguration()
        {
            #region Table

            ToTable("Campaign", DbSchemas.Service);

            #endregion

            #region Properties

            HasKey(c => c.Id);
            Property(c => c.Id).IsRequired().HasColumnName("CampaignId").HasColumnType("INT").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(c => c.Title).IsRequired().HasColumnName("Title").HasColumnType("VARCHAR").HasMaxLength(150);
            Property(c => c.DiscountAmount).IsRequired().HasColumnName("DiscountAmount").HasColumnType("FLOAT");
            Property(c => c.MinimumRequiredQuantity).IsRequired().HasColumnName("MinimumRequiredQuantity").HasColumnType("FLOAT");
            Property(c => c.DiscountType).IsRequired().HasColumnName("DiscountTypeId").HasColumnType("INT");

            #endregion
        }
    }
}