﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Globals.Statics;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Core.Logic.Model.Database.Configurations
{
    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            #region Table

            ToTable("Product", DbSchemas.Service);

            #endregion

            #region Properties

            HasKey(c => c.Id);
            Property(c => c.Id).IsRequired().HasColumnName("ProductId").HasColumnType("INT").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(c => c.Title).IsRequired().HasColumnName("Title").HasColumnType("VARCHAR").HasMaxLength(150);
            Property(c => c.Price).IsRequired().HasColumnName("Price").HasColumnType("FLOAT");

            #endregion
        }
    }
}