﻿using System;

namespace Core.Logic.Model.Entities
{
    public class CategoryProductMap : EntityBase<Guid>
    {
        #region Properties - Mapped

        public int CategoryId { get; set; }
        public int ProductId { get; set; }

        #endregion

        #region Properties - Virtual

        public virtual Category Category { get; set; }
        public virtual Product Product { get; set; }

        #endregion
    }
}