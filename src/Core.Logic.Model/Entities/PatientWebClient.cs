﻿using System;
using System.Net;

namespace Core.Logic.Model.Entities
{
    public class PatientWebClient : WebClient
    {
        #region Fields

        private readonly int _timeout;

        #endregion

        #region Constructors

        public PatientWebClient() : this(100)
        {
        }
        public PatientWebClient(int timeout)
        {
            _timeout = timeout;
        }

        #endregion

        #region Methods - Protected

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var w = base.GetWebRequest(uri);
            if (w != null)
            {
                w.Timeout = _timeout * 1000;
            }
            return w;
        }

        #endregion
    }
}