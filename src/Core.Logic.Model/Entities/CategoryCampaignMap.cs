﻿using System;

namespace Core.Logic.Model.Entities
{
    public class CategoryCampaignMap : EntityBase<Guid>
    {
        #region Properties - Mapped

        public int CategoryId { get; set; }
        public int CampaignId { get; set; }

        #endregion

        #region Properties - Virtual

        public virtual Category Category { get; set; }
        public virtual Campaign Campaign { get; set; }

        #endregion
    }
}