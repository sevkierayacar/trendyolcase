﻿namespace Core.Logic.Model.Entities
{
    public class CartItem
    {
        #region Properties

        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public double FinalPrice { get; set; }

        public Category Category { get; set; }
        public Product Product { get; set; }

        #endregion
    }
}