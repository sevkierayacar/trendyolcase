﻿using Core.Logic.Model.Enums;

namespace Core.Logic.Model.Entities
{
    public class Coupon : EntityBase<int>
    {
        #region Properties - Mapped

        public string Title { get; set; }
        public double DiscountAmount { get; set; }
        public double MinimumPrice { get; set; }
        public DiscountType DiscountType { get; set; }

        #endregion
    }
}