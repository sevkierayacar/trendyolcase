﻿using System.Collections.Generic;

namespace Core.Logic.Model.Entities
{
    public class Category : EntityBase<int>
    {
        #region Properties - Mapped

        public string Title { get; set; }
        public int? ParentId { get; set; }

        #endregion

        #region Properties - Virtual

        public virtual Category Parent { get; set; }
        public virtual HashSet<Category> Children { get; set; }
        public virtual HashSet<CategoryCampaignMap> CategoryCampaignMaps { get; set; }
        public virtual HashSet<CategoryProductMap> CategoryProductMaps { get; set; }

        #endregion
    }
}