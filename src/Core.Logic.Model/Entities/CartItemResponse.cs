﻿namespace Core.Logic.Model.Entities
{
    public class CartItemResponse
    {
        #region Properties

        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public double FinalPrice { get; set; }

        public string CategoryTitle { get; set; }
        public double ProductPrice { get; set; }
        public string ProductTitle { get; set; }

        #endregion
    }
}