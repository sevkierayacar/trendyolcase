﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Logic.Model.Entities
{
    public class Product : EntityBase<int>
    {
        #region Properties - Mapped

        public string Title { get; set; }
        public double Price { get; set; }

        #endregion

        #region Properties - NotMapped

        [NotMapped]
        public string CategoryName { get; set; }

        #endregion
    }
}