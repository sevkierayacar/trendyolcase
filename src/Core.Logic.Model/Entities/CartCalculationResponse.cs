﻿using System.Collections.Generic;
using Service.Api.AppCode;

namespace Core.Logic.Model.Entities
{
    public class CartCalculationResponse : ResponseBase
    {
        public double FinalTotalPrice { get; set; }
        public double TotalAppliedDiscount { get; set; }
        public double DeliveryPrice { get; set; }
        public List<CartItemResponse> CartItems { get; set; }

        public CartCalculationResponse()
        {
            CartItems = new List<CartItemResponse>();
        }
    }
}