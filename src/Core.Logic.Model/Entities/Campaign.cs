﻿using Core.Logic.Model.Enums;

namespace Core.Logic.Model.Entities
{
    public class Campaign : EntityBase<int>
    {
        #region Properties - Mapped

        public string Title { get; set; }
        public double DiscountAmount { get; set; }
        public double MinimumRequiredQuantity { get; set; }
        public DiscountType DiscountType { get; set; }

        #endregion
    }
}
