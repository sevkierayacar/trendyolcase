﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Logic.Model.Interfaces;

namespace Core.Logic.Model.Entities
{
    [Serializable]
    public abstract class EntityBase<T> : IEntity<T>
    {
        #region Properties - Mapped - IEntity

        public T Id { get; set; }

        [Column("CreatedOn", TypeName = "DATETIME2")]
        public DateTime CreatedOn { get; set; }

        [Column("ModifiedOn", TypeName = "DATETIME2")]
        public DateTime ModifiedOn { get; set; }

        #endregion

        #region Constructors

        protected EntityBase()
        {
            CreatedOn = DateTime.UtcNow;
            ModifiedOn = DateTime.UtcNow;
        }

        #endregion

        #region Methods - IEntity

        public virtual bool IsValid()
        {
            return !EqualityComparer<T>.Default.Equals(Id, default(T));
        }

        #endregion
    }
}