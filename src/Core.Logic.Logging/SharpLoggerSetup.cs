﻿using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository;
using log4net.Repository.Hierarchy;

namespace Core.Logic.Logging
{
    internal static class SharpLoggerSetup
    {
        private static string _filePath;

        internal static void Setup(ILoggerRepository loggerRepository, string filePath, params LogType[] logTypes)
        {
            _filePath = filePath;
            Setup(loggerRepository, logTypes);
        }

        internal static void Setup(ILoggerRepository loggerRepository, params LogType[] logTypes)
        {
            Hierarchy hierarchy = (Hierarchy)loggerRepository;

            foreach (var logType in logTypes)
            {
                AddAppender(logType, CreateFileAppender(logType));
            }

            hierarchy.Root.Level = Level.All;
            hierarchy.Configured = true;
        }

        private static IAppender CreateFileAppender(LogType logType)
        {
            if (string.IsNullOrWhiteSpace(_filePath))
                _filePath = "Logs";

            RollingFileAppender result = new RollingFileAppender
            {
                Name = $"{logType.ToString()}_FileAppender",
                File = $@"{_filePath}\{logType.ToString()}.log",
                AppendToFile = true,
                MaxSizeRollBackups = 200,
                RollingStyle = RollingFileAppender.RollingMode.Composite,
                LockingModel = new FileAppender.MinimalLock()
            };

            PatternLayout layout = new PatternLayout
            {
                ConversionPattern = "%date{yyyy-MM-dd HH:mm:ss.fff} [%thread] [%property{Context}] %-5level %logger - %message%newline"
            };
            layout.ActivateOptions();

            result.Layout = layout;
            result.ActivateOptions();

            return result;
        }
        private static void AddAppender(LogType logType, IAppender appender)
        {
            var log = LogManager.GetLogger(logType.ToString());
            var l = (log4net.Repository.Hierarchy.Logger)log.Logger;

            l.AddAppender(appender);
        }
    }
}