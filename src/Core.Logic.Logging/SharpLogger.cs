﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using log4net;
using log4net.Repository;

namespace Core.Logic.Logging
{
    public static class SharpLogger
    {
        #region Fields

        private static int _stackFrameCount;
        private const int MinimumStackFrameCount = 2;
        private static readonly ILoggerRepository LoggerRepository = LogManager.GetRepository();

        #endregion

        #region Methods - Public

        public static void Start(params LogType[] logTypes)
        {
            Start(string.Empty, MinimumStackFrameCount, logTypes);
        }
        public static void Start(string filePath, params LogType[] logTypes)
        {
            Start(filePath, MinimumStackFrameCount, logTypes);
        }
        public static void Start(string filePath, int stackFrameCount = MinimumStackFrameCount, params LogType[] logTypes)
        {
            _stackFrameCount = stackFrameCount;

            var logTypeList = logTypes?.Select(c => c).ToList() ?? new List<LogType>();
            if (!logTypeList.Any())
                logTypeList.AddRange(Enum.GetValues(typeof(LogType)).Cast<LogType>());
            
            SharpLoggerSetup.Setup(LoggerRepository, filePath, logTypeList.ToArray());
        }

        #region Logging Info

        public static void LogInfo(string message, object customObj = null)
        {
            Log(LogType.Info, _stackFrameCount, message, customObj);
        }

        #endregion

        #region Logging Debug

        public static void LogDebug(string message, object customObj = null)
        {
            Log(LogType.Debug, _stackFrameCount, message, customObj);
        }

        #endregion

        #region Logging Warning

        public static void LogWarning(string message, object customObj = null)
        {
            Log(LogType.Warning, _stackFrameCount, message, customObj);
        }

        #endregion

        #region Logging Error

        public static void LogError(Exception ex, object customObj = null)
        {
            Log(LogType.Error, _stackFrameCount, GetExceptionMessage(ex).ToString(), customObj);
        }
        public static void LogError(Exception ex, string message, object customObj = null)
        {
            Log(LogType.Error, _stackFrameCount, $"{GetExceptionMessage(ex)} | {message}", customObj);
        }

        #endregion

        #endregion

        #region Methods - Private

        private static void Log(LogType logType, int stackFrameCount, string message, params object[] customObjs)
        {
            ILog logger = LogManager.GetLogger(logType.ToString());

            if (logger != null)
            {
                StackFrame frame = new StackFrame(stackFrameCount);

                string msg = !string.IsNullOrWhiteSpace(message) ? "Message: " + message : null;
                string customObj = customObjs != null && customObjs.Any() && customObjs.All(c => c != null) ? "CustomObj: " + string.Join(" | ", customObjs.Select(c => c.ToJson())) : null;

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(FormatMethodName(frame));
                var ip = GetIpAddress();
                if(!string.IsNullOrWhiteSpace(ip))
                    sb.AppendLine("Ip: " + ip);

                if (!string.IsNullOrWhiteSpace(msg))
                    sb.AppendLine(msg);
                if (!string.IsNullOrWhiteSpace(customObj))
                    sb.AppendLine(customObj);

                var sbString = sb.ToString();

                switch (logType)
                {
                    case LogType.Debug:
                        logger.Debug(sbString);
                        break;
                    case LogType.Info:
                        logger.Info(sbString);
                        break;
                    case LogType.Warning:
                        logger.Warn(sbString);
                        break;
                    case LogType.Error:
                        logger.Error(sbString);
                        break;
                }
            }
        }
        private static StringBuilder GetExceptionMessage(Exception ex, StringBuilder message = null, int lineCount = 1)
        {
            if (message == null)
                message = new StringBuilder();

            if (lineCount == 1)
                message.AppendLine("");
            
            message.AppendLine(lineCount + ") " +
                               ex.GetType() +
                               (!string.IsNullOrWhiteSpace(ex.Message) ? " | Message: " + ex.Message : string.Empty) +
                               (!string.IsNullOrWhiteSpace(ex.StackTrace) ? " | StackTrace: " + ex.StackTrace : string.Empty)
            );
            if (ex.InnerException != null)
            {
                return GetExceptionMessage(ex.InnerException, message, ++lineCount);
            }

            return message;
        }
        private static string FormatMethodName(StackFrame stackFrame)
        {
            MethodBase method = stackFrame.GetMethod();

            if (method != null)
            {
                Type declaringType = method.DeclaringType;

                if (declaringType != null)
                    return $"{declaringType.Namespace}.{declaringType.Name}.{method.Name}";
            }

            return $"{(method != null ? method.Name : "NULL")}";
        }
        private static string GetIpAddress()
        {
            var result = string.Empty;

            try
            {
                if (HttpContext.Current == null)
                    return "127.0.0.1";

                result = HttpContext.Current.Request.ServerVariables["HTTP_CLIENTIP"];

                if (!string.IsNullOrEmpty(result))
                {
                    string[] addresses = result.Split(',');
                    if (addresses.Length != 0)
                    {
                        result = addresses[0];
                    }
                }

                if (string.IsNullOrWhiteSpace(result))
                    result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrWhiteSpace(result))
                {
                    result = result.Split(',')[0];
                }

                if (string.IsNullOrWhiteSpace(result))
                    result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                if (result == "::1")
                    result = "127.0.0.1";
            }
            catch (Exception)
            {
                //ignored
            }

            return result;
        }

        #endregion
    }
}