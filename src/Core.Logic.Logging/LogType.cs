﻿namespace Core.Logic.Logging
{
    public enum LogType
    {
        Error,
        Info,
        Warning,
        Debug
    }
}