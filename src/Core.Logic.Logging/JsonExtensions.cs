﻿using Newtonsoft.Json;

namespace Core.Logic.Logging
{
    public static class JsonExtensions
    {
        public static T FromJson<T>(this string jsonString)
        {
            return (T)JsonConvert.DeserializeObject(jsonString, typeof(T));
        }
        public static string ToJson(this object obj)
        {
            if (obj != null)
                return JsonConvert.SerializeObject(obj);

            return string.Empty;
        }
    }
}