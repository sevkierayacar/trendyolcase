﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace UI.Console
{
    public class CartSimulator
    {
        private readonly IWebClientFactory _webClientFactory;
        private readonly JsonSerializerSettings _jsonSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All,
            Formatting = Formatting.Indented,
            NullValueHandling = NullValueHandling.Include,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
        };

        public CartSimulator(IWebClientFactory webClientFactory)
        {
            _webClientFactory = webClientFactory;
        }

        public CartCalculationResponse RunCartItems()
        {
            var result = new CartCalculationResponse();
            var cartItems = new List<CartItem>
            {
                new CartItem{ProductId = 1, Quantity = 60},
                new CartItem{ProductId = 3, Quantity = 5560},
                new CartItem{ProductId = 5, Quantity = 780},
                new CartItem{ProductId = 5, Quantity = 1},
                new CartItem{ProductId = 4, Quantity = 221},
                new CartItem{ProductId = 7, Quantity = 3},
            };

            try
            {
                using (var webClient = _webClientFactory.CreateWebClientManager())
                {
                    webClient.PatientWebClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");

                    var jsonReq = JsonConvert.SerializeObject(cartItems);
                    var rsp = webClient.UploadString("http://localhost:2160/Cart/CalculateCart", "POST", jsonReq);
                    result = JsonConvert.DeserializeObject<CartCalculationResponse>(rsp, _jsonSettings);
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }

            return result;
        }
    }
}
