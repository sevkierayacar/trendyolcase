﻿using Core.Logic.Logging;
using Core.Logic.Model.Interfaces;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Extensions.Factory;
using System;

namespace UI.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            SharpLogger.Start();
            SharpLogger.LogInfo("App started!");

            var simulator = GetSimulator();

            var result = simulator.RunCartItems();

            System.Console.WriteLine($"DeliveryPrice: {result.DeliveryPrice}");
            System.Console.WriteLine($"FinalTotalPrice: {result.FinalTotalPrice}");
            System.Console.WriteLine($"TotalAppliedDiscount: {result.TotalAppliedDiscount}");
            System.Console.WriteLine("Products:");
            foreach (var resultProduct in result.CartItems)
            {
                System.Console.WriteLine($"CategoryName: {resultProduct.CategoryTitle} | ProductName: {resultProduct.ProductTitle} | Quantity: {resultProduct.Quantity} | UnitPrice: {resultProduct.ProductPrice} | TotalPrice: {resultProduct.FinalPrice}");
            }
            System.Console.ReadLine();
        }

        #region Methods - Private

        private static CartSimulator GetSimulator()
        {
            var kernel = CreateKernel();

            return new CartSimulator(
                kernel.Get<IWebClientFactory>()
            );
        }
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind(c =>
                    c.FromAssembliesMatching("Core.*.dll")
                        .SelectAllIncludingAbstractClasses()
                        .InheritedFrom<INinject>()
                        .BindAllInterfaces());

                kernel.Bind<IWebClientFactory>().ToFactory();

                return kernel;
            }
            catch (Exception ex)
            {
                SharpLogger.LogError(ex);
                kernel.Dispose();
                throw;
            }
        }

        #endregion
    }
}
