﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Exceptions;
using Core.Logic.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Logic.Repository
{
    public class CouponRepository : ICouponRepository
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public CouponRepository(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - Public - ICategoryRepository

        public async Task<ICollection<Coupon>> FilterByHighestCoupon(double currentPrice)
        {
            try
            {
                return await _context.Coupons
                    .OrderByDescending(c => c.MinimumPrice)
                    .Where(c => c.MinimumPrice <= currentPrice)
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Cannot get products!", ex);
            }
        }

        #endregion
    }
}
