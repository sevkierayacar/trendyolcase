﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Logic.Model.Entities;
using Core.Logic.Model.Exceptions;
using Core.Logic.Model.Interfaces;

namespace Core.Logic.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public CategoryRepository(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - Public - ICategoryRepository

        public async Task<Category> FilterByProductIdAsync(int productId)
        {
            try
            {
                var categoryIds = await _context.CategoryProductMaps
                    .Where(c => c.ProductId == productId)
                    .Select(c => c.CategoryId)
                    .ToListAsync();

                return await _context.Categories
                    .Include(c => c.Parent)
                    .Include(c => c.CategoryProductMaps.Select(x => x.Product))
                    .Include(c => c.CategoryCampaignMaps.Select(x => x.Campaign))
                    .Where(c => categoryIds.Any(p => p == c.Id))
                    .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Cannot get products!", ex);
            }
        }

        #endregion
    }
}
