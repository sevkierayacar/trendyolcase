﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Exceptions;
using Core.Logic.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Logic.Repository
{
    public class ProductRepository : IProductRepository
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public ProductRepository(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - Public - ICategoryRepository

        public async Task<ICollection<Product>> Filter(IEnumerable<int> productIds)
        {
            try
            {
                return await _context.Products
                    .Where(c => productIds.Any(p => p == c.Id))
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Cannot get products!", ex);
            }
        }

        #endregion
    }
}
