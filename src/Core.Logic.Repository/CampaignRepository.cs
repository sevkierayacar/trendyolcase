﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Core.Logic.Model.Entities;
using Core.Logic.Model.Exceptions;
using Core.Logic.Model.Interfaces;
using Z.EntityFramework.Plus;

namespace Core.Logic.Repository
{
    public class CampaignRepository : ICampaignRepository
    {
        #region Fields

        private readonly IMainDataContext _context;

        #endregion

        #region Constructors

        public CampaignRepository(IMainDataContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods - Public - ICategoryRepository

        public async Task<ICollection<Campaign>> Filter(IEnumerable<int> productIds)
        {
            try
            {
                var categoryIds = await _context.CategoryProductMaps
                    .Where(c => productIds.Any(p => p == c.ProductId))
                    .Select(c => c.CategoryId)
                    .ToListAsync();

                return await _context.CategoryCampaignMaps
                    .Include(c => c.Campaign)
                    .Where(c => categoryIds.Any(p => p == c.CategoryId))
                    .Select(c => c.Campaign)
                    .ToListAsync();
            }
            catch (Exception ex)
            {
                throw new RepositoryException("Cannot get products!", ex);
            }
        }

        #endregion
    }
}
