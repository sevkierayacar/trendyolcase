﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Logic.Model.Enums;
using Core.Logic.Model.Exceptions;

namespace Core.Logic.Manager
{
    public class CartManager : ICartManager
    {
        #region Fields

        private readonly IProductRepository _productRepository;
        private readonly ICampaignRepository _campaignRepository;
        private readonly ICouponRepository _couponRepository;

        #endregion

        #region Constructors

        public CartManager(IProductRepository productRepository, ICampaignRepository campaignRepository, ICouponRepository couponRepository)
        {
            _productRepository = productRepository;
            _campaignRepository = campaignRepository;
            _couponRepository = couponRepository;
        }

        #endregion

        #region Methods - Public - ICartManager

        public async Task<ICollection<CartItem>> ApplyCampaigns(ICollection<CartItem> cartItems)
        {
            try
            {
                var products = await _productRepository.Filter(cartItems.Select(c => c.ProductId));

                var items = products
                    .Select(c =>
                        new
                        {
                            Product = c,
                            CartItem = cartItems.First(p => p.ProductId == c.Id)
                        })
                    .ToList();
                foreach (var item in items)
                {
                    var campaigns = (await _campaignRepository.Filter(cartItems.Select(c => c.ProductId))) //Get campa
                        .OrderByDescending(c => c.MinimumRequiredQuantity)
                        .Where(c => item.CartItem.Quantity >= c.MinimumRequiredQuantity)
                        .ToList();

                    var campaign = GetBestCampaign(item.Product, item.CartItem.Quantity, campaigns);

                    item.CartItem.FinalPrice = CalculateDiscountedPrice(item.Product.Price, item.CartItem.Quantity, campaign);
                }

                return cartItems;
            }
            catch (Exception ex)
            {
                throw new ManagerException("Cannot apply campaigns", ex);
            }
        }

        public async Task<ICollection<CartItem>> ApplyCoupons(ICollection<CartItem> cartItems)
        {
            try
            {
                foreach (var item in cartItems)
                {
                    var coupons = await _couponRepository.FilterByHighestCoupon(item.FinalPrice);
                    var coupon = GetBestCoupon(item.FinalPrice, coupons);

                    item.FinalPrice = CalculateDiscountedPrice(item.FinalPrice, coupon);
                }

                return cartItems;
            }
            catch (Exception ex)
            {
                throw new ManagerException("Cannot apply coupons", ex);
            }
        }

        public double GetDeliveryPrice(double costPerDelivery, double costPerProduct, double fixedPrice, ICollection<CartItem> cartItems)
        {
            var numberOfDeliveries = cartItems.Select(c => c.Category.Id).Distinct().Count();
            var numberOfProducts = cartItems.Select(c => c.ProductId).Distinct().Count();

            return (costPerDelivery * numberOfDeliveries) + (costPerProduct * numberOfProducts) + fixedPrice;
        }

        #endregion

        #region Methods - Private
        
        //Gets the lowest price possible
        private Campaign GetBestCampaign(Product product, int quantity, List<Campaign> campaigns)
        {
            var discountedPrices = new List<Tuple<double, Campaign>>();

            foreach (var campaign in campaigns)
            {
                var discount = CalculateDiscountedPrice(product.Price, quantity, campaign);
                discountedPrices.Add(new Tuple<double, Campaign>(discount, campaign));
            }

            return discountedPrices
                .OrderBy(c => c.Item1)
                .FirstOrDefault()?
                .Item2;
        }
        //Gets the lowest price possible
        private Coupon GetBestCoupon(double price, ICollection<Coupon> coupons)
        {
            var discountedPrices = new List<Tuple<double, Coupon>>();

            foreach (var coupon in coupons)
            {
                var discount = CalculateDiscountedPrice(price, coupon);
                discountedPrices.Add(new Tuple<double, Coupon>(discount, coupon));
            }

            return discountedPrices
                .OrderBy(c => c.Item1)
                .FirstOrDefault()?
                .Item2;
        }
        private double CalculateDiscountedPrice(double price, int quantity, Campaign campaign)
        {
            var result = quantity * price;

            if (campaign == null)
                return result;

            switch (campaign.DiscountType)
            {
                case DiscountType.Rate:
                    result = result - ((result * campaign.DiscountAmount) / 100);
                    break;
                case DiscountType.Amount:
                    result = result - campaign.DiscountAmount;
                    break;
            }

            if (result < 0)
                result = 0;

            return result;
        }
        private double CalculateDiscountedPrice(double price, Coupon coupon)
        {
            var result = price;

            if (coupon == null)
                return result;

            switch (coupon.DiscountType)
            {
                case DiscountType.Rate:
                    result = result - ((result * coupon.DiscountAmount) / 100);
                    break;
                case DiscountType.Amount:
                    result = result - coupon.DiscountAmount;
                    break;
            }

            if (result < 0)
                result = 0;

            return result;
        }

        #endregion
    }
}
