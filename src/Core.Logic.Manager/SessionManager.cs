﻿using System.Web;
using Core.Logic.Model.Interfaces;

namespace Core.Logic.Manager
{
    public class SessionManager : ISessionManager
    {
        #region Properties

        public string Username
        {
            get
            {
                if ((string)HttpContext.Current.Session["username"] != null)
                    return (string)HttpContext.Current.Session["username"];

                return null;
            }
            set
            {
                HttpContext.Current.Session["username"] = value;
            }
        }

        #endregion
    }
}