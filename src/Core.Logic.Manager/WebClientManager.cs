﻿using Core.Logic.Model.Entities;
using Core.Logic.Model.Interfaces;
using System.Threading.Tasks;

namespace Core.Logic.Manager
{
    public class WebClientManager : IWebClientManager
    {
        #region Fields

        private readonly PatientWebClient _patientWebClient;

        #endregion

        #region Properties

        public PatientWebClient PatientWebClient => _patientWebClient;

        #endregion

        #region Constructors

        public WebClientManager() : this(100)
        {

        }
        public WebClientManager(int timeout)
        {
            _patientWebClient = new PatientWebClient(timeout);
        }

        #endregion

        #region Methods - Public - IWebClientManager

        public string UploadString(string address, string method, string data)
        {
            return _patientWebClient.UploadString(address, method, data);
        }

        #endregion

        #region Methods - Public - IDisposable

        public void Dispose()
        {
            _patientWebClient?.Dispose();
        }

        #endregion
    }
}
