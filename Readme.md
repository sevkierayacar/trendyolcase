#TrendyolCase

### Getting Started

These instructions will help you to deploy and run as well as provide some insight on project's architecture.

### Assumptions

`CostPerDelivery` and `CostPerProduct` values are given static values within the code.

### Running

In order to run without problem please follow these few steps:
1. Check `ConnStrings.config` file wihtin **Service.Api** and write a suitable connection string. As default, a local MSSQLExpress is used.
1. Set `Service.Api` as **Startup Project**
1. Open `Package Manager Tool` and select `Core.Logic.Model` as **Default Project**
1. Within `Package Manager Tool` run this command: update-database

IMPORTANT: There is **seeding** method within the configurations. Please run migration **ONLY ONCE**! Because, even though I used **AddOrUpdate**, some mapping tables have Auto-Generated identification fields and this can be multiplicated if the migration is run more than once.

After these steps above please follow this last step:
1. From Visual Studio, `UI.Console` and `Service.Api` projects must be BOTH set as **Startup**. Then, can simply be started (either with or without debugging) from Visual Studio. 
1. Since the data is generated by migration seed, the moment Console application is run, a set of result will be displayed automatically

### Logging

In this solution, I mainly used Error, Debug, and Info logs depending on the action. There certainly could be more logging but for this project, I'd like to believe that it is enough.
The logs can be found within the application's startup path inside a folder named as `Logs`.
There are 4 categories of logging level:

1. ProcessInfo
1. Debug
1. Error
1. Warning

Any of them can be activated or deactivated.
You can find logging setup in `Global.asax.cs` file as below:
```
SharpLogger.Start(2, LogType.ProcessInfo, LogType.Debug, LogType.Error, LogType.Warning);
```
The types are `params[]LogTypes`, so the logs can be reduced as reuqired.

### Solution's Brief Explanations

- **Core.Logic.Logging**: Used for easy logging that depends on log4net.
- **Core.Logic.Model**: This is the most important core project of the solution which includes Interfaces, Entities, Enums, some helpers, and extensions. No logic is implemented in this dll.
- **Core.Logic.Repository**: Database filtering are done here.
- **Service.Api**: MVC5 API application for handling restful requests.
- **UI.Console**: Designed as a client which consumes **Service.Api**
